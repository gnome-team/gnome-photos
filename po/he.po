# Hebrew translations for PACKAGE package.
# Copyright (C) 2013 THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Yaron Shahrabani <sh.yaron@gmail.com>, 2013.
# Yosef Or Boczko <yoseforb@gmail.com>, 2013-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: GNOME Photos\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-photos/issues\n"
"POT-Creation-Date: 2021-05-06 11:22+0000\n"
"PO-Revision-Date: 2021-06-21 21:15+0300\n"
"Last-Translator: Yaron Shahrabani <sh.yaron@gmail.com>\n"
"Language-Team: Hebrew <yoseforb@gmail.com>\n"
"Language: he\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n==2 ? 1 : n>10 && n%10==0 ? "
"2 : 3);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 3.0\n"

#: data/org.gnome.Photos.appdata.xml.in:7 data/org.gnome.Photos.desktop.in.in:3
#: src/photos-application.c:3030 src/photos-embed.c:822
#: src/photos-main-window.c:437 src/photos-search-type-manager.c:101
msgid "Photos"
msgstr "תמונות"

#: data/org.gnome.Photos.appdata.xml.in:8 src/photos-main-window.c:457
msgid "Access, organize and share your photos on GNOME"
msgstr "לגשת, לארגן ולשתף את התמונות שלך ב־GNOME"

#: data/org.gnome.Photos.appdata.xml.in:10
msgid ""
"Access, organize and share your photos on GNOME. A simple and elegant "
"replacement for using a file manager to deal with photos. Enhance, crop and "
"edit in a snap. Seamless cloud integration is offered through GNOME Online "
"Accounts."
msgstr ""
"גישה, ארגון ושיתוף התמונות שלך על GNOME. מהווה תחליף פשוט ומהודר לשימוש "
"בסייר קבצים להתמודדות עם תמונות. נתן להגדיל, לחתוך ולערוך בלחיצה. שילוב חלק "
"של ענן מוצע באמצעות GNOME חשבונות מקוונים."

#: data/org.gnome.Photos.appdata.xml.in:16
msgid "You can:"
msgstr "מאפשר לך:"

#: data/org.gnome.Photos.appdata.xml.in:18
msgid "Automatically find all your pictures"
msgstr "איתור אוטומטי של כל התמונות שלך"

#: data/org.gnome.Photos.appdata.xml.in:19
msgid "View recent local and online photos"
msgstr "הצגת תמונות מקומיות ומקוונות אחרונות"

#: data/org.gnome.Photos.appdata.xml.in:20
msgid "Access your Facebook or Flickr pictures"
msgstr "גישה לתמונות Facebook או Flickr שלך"

#: data/org.gnome.Photos.appdata.xml.in:21
msgid ""
"View photos on TVs, laptops or other DLNA renderers on your local network"
msgstr "הצגת תמונות בטלוויזיה, במחשב הנייד או במעבד DLNA אחר ברשת המקומית שלך"

#: data/org.gnome.Photos.appdata.xml.in:23
msgid "Set pictures as your desktop background"
msgstr "הגדרת תמונות כרקע שולחן העבודה שלך"

#: data/org.gnome.Photos.appdata.xml.in:24
msgid "Print photos"
msgstr "הדפסת תמונות"

#: data/org.gnome.Photos.appdata.xml.in:25
msgid "Select favorites"
msgstr "בחירת מועדפים"

#: data/org.gnome.Photos.appdata.xml.in:26
msgid ""
"Easily edit your pictures in the app, or send to a full featured editor for "
"more advanced changes"
msgstr ""
"עריכה פשוטה של תמונות ביישום, או פתיחה של עורך עשיר בתכונות לשינויים מתקדמים "
"יותר"

#: data/org.gnome.Photos.appdata.xml.in:99
msgid "The GNOME Project"
msgstr "מיזם GNOME"

#: data/org.gnome.Photos.desktop.in.in:4
msgid "Access, organize and share photos"
msgstr "לגשת, לארגן ולשתף תמונות"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.Photos.desktop.in.in:15
msgid "Photos;Pictures;"
msgstr "תמונות;תצלומים;צילומים;"

#: data/org.gnome.photos.gschema.xml:5
msgid "Window size"
msgstr "Window size"

#: data/org.gnome.photos.gschema.xml:6
msgid "Window size (width and height)."
msgstr "Window size (width and height)."

#: data/org.gnome.photos.gschema.xml:10
msgid "Window position"
msgstr "Window position"

#: data/org.gnome.photos.gschema.xml:11
msgid "Window position (x and y)."
msgstr "Window position (x and y)."

#: data/org.gnome.photos.gschema.xml:15
msgid "Window maximized"
msgstr "Window maximized"

#: data/org.gnome.photos.gschema.xml:16
msgid "Window maximized state"
msgstr "Window maximized state"

#: src/photos-application.c:168
msgid "Show the empty state"
msgstr "Show the empty state"

#: src/photos-application.c:169
msgid "Show the application's version"
msgstr "Show the application's version"

#. Translators: this is the default sub-directory where photos will
#. * be imported.
#.
#. Translators: this is the default sub-directory where photos
#. * will be exported.
#.
#. Translators: this is the name of the default album that will be
#. * created for imported photos.
#.
#: src/photos-application.c:1310 src/photos-export-dialog.c:195
#: src/photos-import-dialog.c:134
msgid "%-d %B %Y"
msgstr "%-d ב%B %Y"

#: src/photos-base-item.c:1034
msgid "Album"
msgstr "אלבום"

#: src/photos-base-item.c:2862
msgid "Screenshots"
msgstr "צילומי מסך"

#: src/photos-delete-notification.c:156
#, c-format
msgid "“%s” deleted"
msgstr "‏„%s” נמחק"

#: src/photos-delete-notification.c:159
#, c-format
msgid "%d item deleted"
msgid_plural "%d items deleted"
msgstr[0] "פריט אחד נמחק"
msgstr[1] "שני פריטים נמחקו"
msgstr[2] "‫%d פריטים נמחקו"
msgstr[3] "‫%d פריטים נמחקו"

#: src/photos-delete-notification.c:166 src/photos-done-notification.c:119
msgid "Undo"
msgstr "ביטול"

#: src/photos-dlna-renderers-dialog.ui:7
msgid "DLNA Renderer Devices"
msgstr "התקן עיבוד DLNA"

#: src/photos-done-notification.c:112
#, c-format
msgid "“%s” edited"
msgstr "‏„%s” נמחק"

#: src/photos-embed.c:827
msgid "Collection View"
msgstr "תצוגת אלבומים"

#: src/photos-embed.c:830 src/photos-search-type-manager.c:83
msgid "Albums"
msgstr "אלבומים"

#: src/photos-embed.c:835 src/photos-search-type-manager.c:92
msgid "Favorites"
msgstr "מועדפים"

#: src/photos-embed.c:840
msgid "Import"
msgstr "יבוא"

#: src/photos-embed.c:843 src/photos-main-toolbar.c:336
msgid "Search"
msgstr "חיפוש"

#: src/photos-empty-results-box.c:116
msgid "No Albums Found"
msgstr "לא נמצאו אלבומים"

#: src/photos-empty-results-box.c:120
msgid "Starred Photos Will Appear Here"
msgstr "תמונות המסומנות בכוכב תופענה כאן"

#: src/photos-empty-results-box.c:130
msgid "No Photos Found"
msgstr "לא נמצאו תמונות"

#: src/photos-empty-results-box.c:155
msgid "You can create albums from the Photos view"
msgstr "נתן ליצור אלבומים מתצוגת התמונות"

#. Translators: this should be translated in the context of
#. * the "Photos from your Online Accounts and Pictures folder
#. * will appear here." sentence below.
#.
#: src/photos-empty-results-box.c:176
msgid "Online Accounts"
msgstr "החשבונות המקוונים"

#. Translators: this should be translated in the context of
#. * the "Photos from your Online Accounts and Pictures folder
#. * will appear here." sentence below.
#.
#: src/photos-empty-results-box.c:183
msgid "Pictures folder"
msgstr "תיקיית התמונות"

#. Translators: the first %s here is "Online Accounts" and the
#. * second %s is "Pictures folder", which are in separate
#. * strings due to markup, and should be translated only in the
#. * context of this sentence.
#.
#: src/photos-empty-results-box.c:190
#, c-format
msgid "Photos from your %s and %s will appear here."
msgstr "תמונות מ%s שלך ומ%s שלך תופענה כאן."

#: src/photos-empty-results-box.c:200
msgid "Try a different search."
msgstr "יש לנסות חיפוש שונה."

#: src/photos-export-dialog.ui:22
msgctxt "dialog title"
msgid "Export"
msgstr "ייצוא"

#: src/photos-export-dialog.ui:38
msgid "_Folder Name"
msgstr "שם _תיקייה"

#: src/photos-export-dialog.ui:66 src/photos-print-setup.c:908
msgid "Size"
msgstr "גודל"

#: src/photos-export-dialog.ui:83
msgid "F_ull"
msgstr "_מלא"

#: src/photos-export-dialog.ui:113
msgid "_Reduced"
msgstr "מ_צומצם"

#: src/photos-export-dialog.ui:160 src/photos-import-dialog.ui:185
#: src/photos-main-toolbar.c:488 src/photos-main-toolbar.c:524
#: src/photos-main-toolbar.c:635
msgid "_Cancel"
msgstr "_ביטול"

#: src/photos-export-dialog.ui:167
msgid "_Export"
msgstr "_ייצוא"

#. Translators: this is the estimated size of the exported image in
#. * the form "1600×1067 (0.6 GB)".
#.
#: src/photos-export-dialog.c:66
#, c-format
msgid "%d×%d (%s)"
msgstr "‏%d×%d ‏(%s)"

#: src/photos-export-dialog.c:252
msgid "Calculating export size…"
msgstr "גודל ייצוא מחושב…"

#: src/photos-export-notification.c:317
msgid "Failed to export: not enough space"
msgstr "הייצוא נכשל: אין מספיק מקום"

#: src/photos-export-notification.c:319
msgid "Failed to export"
msgstr "הייצוא נכשל"

#: src/photos-export-notification.c:326
#, c-format
msgid "“%s” exported"
msgstr "‏„%s” יּוצא"

#: src/photos-export-notification.c:330
#, c-format
msgid "%d item exported"
msgid_plural "%d items exported"
msgstr[0] "פריט אחד ייוצא"
msgstr[1] "שני פריטים ייוצאו"
msgstr[2] "‫%d פריטים ייוצאו"
msgstr[3] "‫%d פריטים ייוצאו"

#: src/photos-export-notification.c:347
msgid "Analyze"
msgstr "אבחון"

#: src/photos-export-notification.c:352
msgid "Empty Trash"
msgstr "פינוי האשפה"

#. Translators: this is the Open action in a context menu
#: src/photos-export-notification.c:369 src/photos-preview-menu.ui:6
#: src/photos-selection-toolbar.c:248 src/photos-selection-toolbar.ui:28
msgid "Open"
msgstr "פתיחה"

#. Translators: this is the label of the button to open the
#. * folder where the item was exported.
#.
#: src/photos-export-notification.c:379
msgid "Export Folder"
msgstr "ייצוא תיקייה"

#. Translators: this is the fallback title in the form
#. * "Facebook — 2nd January 2013".
#.
#: src/photos-facebook-item.c:100 src/photos-flickr-item.c:102
#: src/photos-google-item.c:98
#, c-format
msgid "%s — %s"
msgstr "%s — %s"

#: src/photos-help-overlay.ui:32
msgctxt "shortcut window"
msgid "General"
msgstr "כללי"

#: src/photos-help-overlay.ui:36
msgctxt "shortcut window"
msgid "Show help"
msgstr "הצגת עזרה"

#: src/photos-help-overlay.ui:43
msgctxt "shortcut window"
msgid "Keyboard shortcuts"
msgstr "צירופי מקשים"

#: src/photos-help-overlay.ui:50
msgctxt "shortcut window"
msgid "Quit"
msgstr "יציאה"

#: src/photos-help-overlay.ui:59
msgctxt "shortcut window"
msgid "Navigation"
msgstr "ניווט"

#: src/photos-help-overlay.ui:63
msgctxt "shortcut window"
msgid "Next photo"
msgstr "תמונה הבאה"

#: src/photos-help-overlay.ui:70
msgctxt "shortcut window"
msgid "Previous photo"
msgstr "תמונה קודמת"

#: src/photos-help-overlay.ui:77 src/photos-help-overlay.ui:85
msgctxt "shortcut window"
msgid "Go back"
msgstr "חזרה"

#: src/photos-help-overlay.ui:95
msgctxt "shortcut window"
msgid "Overview"
msgstr "סקירה"

#: src/photos-help-overlay.ui:99
msgctxt "shortcut window"
msgid "Search"
msgstr "חיפוש"

#: src/photos-help-overlay.ui:106
msgctxt "shortcut window"
msgid "Select all"
msgstr "בחירה בהכול"

#: src/photos-help-overlay.ui:113
msgctxt "shortcut window"
msgid "Export selected photo"
msgstr "יצוא תמונות נבחרות"

#: src/photos-help-overlay.ui:120
msgctxt "shortcut window"
msgid "Print selected photo"
msgstr "הדפסת תמונות נבחרות"

#: src/photos-help-overlay.ui:127
msgctxt "shortcut window"
msgid "Delete selected photos"
msgstr "מחיקת תמונות נבחרות"

#: src/photos-help-overlay.ui:136
msgctxt "shortcut window"
msgid "Photo view"
msgstr "תצוגת תמונות"

#: src/photos-help-overlay.ui:140
msgctxt "shortcut window"
msgid "Edit"
msgstr "עריכה"

#: src/photos-help-overlay.ui:147
msgctxt "shortcut window"
msgid "Export"
msgstr "ייצוא"

#: src/photos-help-overlay.ui:154
msgctxt "shortcut window"
msgid "Print"
msgstr "הדפסה"

#: src/photos-help-overlay.ui:161
msgctxt "shortcut window"
msgid "Zoom in"
msgstr "התקרבות"

#: src/photos-help-overlay.ui:168
msgctxt "shortcut window"
msgid "Zoom out"
msgstr "התרחקות"

#: src/photos-help-overlay.ui:175
msgctxt "shortcut window"
msgid "Best fit"
msgstr "ההתאמה הטובה ביותר"

#: src/photos-help-overlay.ui:182
msgctxt "shortcut window"
msgid "Delete"
msgstr "מחיקה"

#: src/photos-help-overlay.ui:189
msgctxt "shortcut window"
msgid "Action menu"
msgstr "תפריט פעולה"

#: src/photos-help-overlay.ui:196
msgctxt "shortcut window"
msgid "Fullscreen"
msgstr "מסך מלא"

#: src/photos-help-overlay.ui:205
msgctxt "shortcut window"
msgid "Edit view"
msgstr "תצוגת עריכה"

#: src/photos-help-overlay.ui:209
msgctxt "shortcut window"
msgid "Cancel"
msgstr "ביטול"

#: src/photos-import-dialog.c:194
msgid "An album with that name already exists"
msgstr "כבר קיים אלבום בשם זה"

#: src/photos-import-dialog.c:211
msgid "An album for that date already exists"
msgstr "כבר קיים אלבום לתאריך זה"

#: src/photos-import-dialog.ui:22
msgctxt "dialog title"
msgid "Name Album"
msgstr "שם אלבום"

#: src/photos-import-dialog.ui:54
msgid "Create _New"
msgstr "יצירת _חדש"

#: src/photos-import-dialog.ui:118
msgid "Add to _Existing"
msgstr "הוספה ל_קיים"

#: src/photos-import-dialog.ui:192 src/photos-organize-collection-dialog.c:69
msgid "_Add"
msgstr "הו_ספה"

#. Translators: %s refers to an online account provider, e.g.,
#. * "Facebook" or "Flickr".
#.
#: src/photos-indexing-notification.c:163
#, c-format
msgid "Fetching photos from %s"
msgstr "שולף תמונות מ־%s"

#: src/photos-indexing-notification.c:166
msgid "Fetching photos from online accounts"
msgstr "שולף תמונות מחשבונות מקוונים"

#: src/photos-indexing-notification.c:193
msgid "Your photos are being indexed"
msgstr "התמונות שלך נשמרות במפתח"

#: src/photos-indexing-notification.c:194
msgid "Some photos might not be available during this process"
msgstr "יתכן שחלק מהתמונות לא תהיינה זמינות במשך תהליך זה"

#: src/photos-item-manager.c:845
#, c-format
msgid "Oops! Unable to load “%s”"
msgstr "אופס! לא נתן לטעון את „%s”"

#: src/photos-local-item.c:241 src/photos-source-manager.c:399
msgid "Local"
msgstr "מקומי"

#: src/photos-main-toolbar.c:80
msgid "Click on items to select them"
msgstr "יש ללחוץ על פריטים כדי לבחור אותם"

#: src/photos-main-toolbar.c:82
#, c-format
msgid "%d selected"
msgid_plural "%d selected"
msgstr[0] "אחת נבחרה"
msgstr[1] "שתיים נבחרו"
msgstr[2] "‫%d נבחרו"
msgstr[3] "‫%d נבחרו"

#: src/photos-main-toolbar.c:170
msgid "Select items for import"
msgstr "בחירת פריטים לייבוא"

#: src/photos-main-toolbar.c:174
#, c-format
msgid "Select items for import (%u selected)"
msgid_plural "Select items for import (%u selected)"
msgstr[0] "בחירת פריטים לייבוא (אחד נבחר)"
msgstr[1] "בחירת פריטים לייבוא (%u נבחרו)"
msgstr[2] "בחירת פריטים לייבוא (%u נבחרו)"
msgstr[3] "בחירת פריטים לייבוא (%u נבחרו)"

#: src/photos-main-toolbar.c:217
msgid "Back"
msgstr "חזרה"

#: src/photos-main-toolbar.c:351
msgid "Select Items"
msgstr "בחירת פריטים"

#. length == 1
#. Translators: this is the Open action in a context menu
#: src/photos-main-toolbar.c:411 src/photos-selection-toolbar.c:245
#: src/photos-share-notification.c:165
#, c-format
msgid "Open with %s"
msgstr "פתיחה באמצעות %s"

#: src/photos-main-toolbar.c:442 src/photos-selection-toolbar.c:254
msgid "Remove from favorites"
msgstr "הסרה מהמועדפים"

#: src/photos-main-toolbar.c:447 src/photos-selection-toolbar.c:259
msgid "Add to favorites"
msgstr "הוספה למועדפים"

#: src/photos-main-toolbar.c:492
msgid "Done"
msgstr "בוצע"

#: src/photos-main-toolbar.c:528
msgid "_Select"
msgstr "_בחירה"

#: src/photos-main-window.c:458
msgid ""
"Copyright © 2012 – 2021 Red Hat, Inc.\n"
"Copyright © 2015 – 2018 Umang Jain"
msgstr ""
"כל הזכויות שמורות © ‎2012 - 2021 Red Hat, Inc.\n"
"‬\n"
"‫כל הזכויות שמורות © ‎2015 - 2018 Umang Jain"

#. Translators: Put your names here
#: src/photos-main-window.c:466
msgid "translator-credits"
msgstr ""
"ירון שהרבני <sh.yaron@gmail.com>\n"
"יוסף אור בוצ׳קו <yoseforb@gmail.com>\n"
"<a href=\"https://l10n.gnome.org/teams/he/\">מיזם תרגום GNOME לעברית</a>"

#: src/photos-organize-collection-dialog.c:70
msgid "_OK"
msgstr "_אישור"

#. Translators: "Organize" refers to photos in this context
#: src/photos-organize-collection-dialog.c:121
msgctxt "Dialog title"
msgid "Organize"
msgstr "ארגון"

#: src/photos-preview-menu.ui:10
msgid "Export…"
msgstr "ייצוא…"

#: src/photos-preview-menu.ui:14
msgid "Print…"
msgstr "הדפסה…"

#: src/photos-preview-menu.ui:19
msgid "Delete"
msgstr "מחיקה"

#: src/photos-preview-menu.ui:23
msgid "Display on…"
msgstr "הצגה…"

#: src/photos-preview-menu.ui:27
msgid "Set as Background"
msgstr "הגדרה כרקע שולחן העבודה"

#: src/photos-preview-menu.ui:31
msgid "Set as Lock Screen"
msgstr "הגדרה כרקע מסך הנעילה"

#: src/photos-preview-menu.ui:37 src/photos-properties-dialog.c:883
#: src/photos-selection-toolbar.ui:101
msgid "Properties"
msgstr "מאפיינים"

#: src/photos-preview-menu.ui:41
msgid "Fullscreen"
msgstr "מסך מלא"

#: src/photos-primary-menu.ui:6
msgid "Keyboard Shortcuts"
msgstr "צירופי מקשים"

#: src/photos-primary-menu.ui:10
msgid "_Help"
msgstr "ע_זרה"

#: src/photos-primary-menu.ui:14
msgid "About Photos"
msgstr "על יישום התמונות"

#: src/photos-print-notification.c:71
#, c-format
msgid "Printing “%s”: %s"
msgstr "מדפיס „%s”:‏ %s"

#: src/photos-print-operation.c:241
msgid "Image Settings"
msgstr "הגדרות התמונה"

#: src/photos-print-setup.c:880
msgid "Position"
msgstr "מיקום"

#: src/photos-print-setup.c:883
msgid "_Left:"
msgstr "_שמאל:"

#: src/photos-print-setup.c:884
msgid "_Right:"
msgstr "י_מין:"

#: src/photos-print-setup.c:885
msgid "_Top:"
msgstr "למ_עלה:"

#: src/photos-print-setup.c:886
msgid "_Bottom:"
msgstr "למ_טה:"

#: src/photos-print-setup.c:888
msgid "C_enter:"
msgstr "מ_רכז:"

#: src/photos-print-setup.c:892
msgid "None"
msgstr "ללא"

#: src/photos-print-setup.c:893
msgid "Horizontal"
msgstr "אופקי"

#: src/photos-print-setup.c:894
msgid "Vertical"
msgstr "אנכי"

#: src/photos-print-setup.c:895
msgid "Both"
msgstr "שניהם"

#: src/photos-print-setup.c:911
msgid "_Width:"
msgstr "_רוחב:"

#: src/photos-print-setup.c:912
msgid "_Height:"
msgstr "_גובה:"

#: src/photos-print-setup.c:914
msgid "_Scaling:"
msgstr "_אמת מידה:"

#: src/photos-print-setup.c:923
msgid "_Unit:"
msgstr "י_חידה:"

#: src/photos-print-setup.c:927
msgid "Millimeters"
msgstr "מילימטרים"

#: src/photos-print-setup.c:928
msgid "Inches"
msgstr "אינצ׳ים"

#: src/photos-print-setup.c:956
msgid "Preview"
msgstr "תצוגה מקדימה"

#: src/photos-properties-dialog.c:246
msgid "Edited in Photos"
msgstr "נערך בתמונות"

#: src/photos-properties-dialog.c:259
msgid "Untouched"
msgstr "לא נגעו בו"

#. Translators: this is the label next to the photo title in the
#. * properties dialog
#.
#: src/photos-properties-dialog.c:442
msgctxt "Document Title"
msgid "Title"
msgstr "כותרת"

#. Translators: this is the label next to the photo author in
#. * the properties dialog
#.
#: src/photos-properties-dialog.c:454
msgctxt "Document Author"
msgid "Author"
msgstr "יוצר"

#: src/photos-properties-dialog.c:461
msgid "Source"
msgstr "מקור"

#: src/photos-properties-dialog.c:467
msgid "Date Modified"
msgstr "תאריך השינוי"

#: src/photos-properties-dialog.c:475
msgid "Date Created"
msgstr "תאריך היצירה"

#. Translators: this is the label next to the photo type in the
#. * properties dialog
#.
#: src/photos-properties-dialog.c:485
msgctxt "Document Type"
msgid "Type"
msgstr "סוג"

#: src/photos-properties-dialog.c:495
msgid "Dimensions"
msgstr "ממדים"

#: src/photos-properties-dialog.c:507
msgid "Location"
msgstr "מיקום"

#: src/photos-properties-dialog.c:525
msgid "Camera"
msgstr "מצלמה"

#: src/photos-properties-dialog.c:535 src/photos-tool-colors.c:365
msgid "Exposure"
msgstr "חשיפה"

#: src/photos-properties-dialog.c:545
msgid "Aperture"
msgstr "צמצם"

#: src/photos-properties-dialog.c:555
msgid "Focal Length"
msgstr "אורך מוקד"

#: src/photos-properties-dialog.c:565
msgid "ISO Speed"
msgstr "מהירות ISO"

#: src/photos-properties-dialog.c:575
msgid "Flash"
msgstr "מבזק"

#: src/photos-properties-dialog.c:584
msgid "Modifications"
msgstr "התאמה"

#: src/photos-properties-dialog.c:656
#, c-format
msgid "%ld × %ld pixel"
msgid_plural "%ld × %ld pixels"
msgstr[0] "‏%ld ‏× %ld פיקסל"
msgstr[1] "‏%ld ‏× %ld פיקסלים"
msgstr[2] "‏%ld ‏× %ld פיקסלים"
msgstr[3] "‏%ld ‏× %ld פיקסלים"

#: src/photos-properties-dialog.c:721
msgid "Off, did not fire"
msgstr "כבוי, לא פועל"

#: src/photos-properties-dialog.c:723
msgid "On, fired"
msgstr "פועל"

#: src/photos-properties-dialog.c:760
msgid "Discard all Edits"
msgstr "השלכת כל השינויים"

#: src/photos-removable-devices-button.ui:47
msgid "Devices"
msgstr "התקנים"

#: src/photos-search-match-manager.c:149 src/photos-search-type-manager.c:74
#: src/photos-source-manager.c:396
msgid "All"
msgstr "הכול"

#. Translators: "Title" refers to "Match Title" when searching.
#: src/photos-search-match-manager.c:155
msgctxt "Search Filter"
msgid "Title"
msgstr "כותרת"

#. Translators: "Author" refers to "Match Author" when searching.
#: src/photos-search-match-manager.c:161
msgctxt "Search Filter"
msgid "Author"
msgstr "יוצר"

#. Translators: this is a verb that refers to "All", "Title" and
#. * "Author", as in "Match All", "Match Title" and "Match Author".
#.
#: src/photos-search-match-manager.c:213
msgid "Match"
msgstr "התאמה"

#. Translators: "Type" refers to a search filter. eg., All, Albums,
#. * Favorites and Photos.
#.
#: src/photos-search-type-manager.c:126
msgctxt "Search Filter"
msgid "Type"
msgstr "סוג"

#: src/photos-selection-menu.ui:6
msgid "Select All"
msgstr "בחירת הכול"

#: src/photos-selection-menu.ui:11
msgid "Select None"
msgstr "ביטול הבחירה"

#: src/photos-selection-toolbar.ui:19
msgid "Export"
msgstr "ייצוא"

#: src/photos-selection-toolbar.ui:111
msgid "Add to Album"
msgstr "הוספה לאלבום"

#: src/photos-share-dialog.ui:25
msgctxt "dialog title"
msgid "Share"
msgstr "שיתוף"

#: src/photos-share-notification.c:148
#, c-format
msgid "“%s” shared"
msgstr "‏„%s” שוּתף"

#: src/photos-share-point-email.c:78
msgid "E-Mail"
msgstr "דוא״ל"

#: src/photos-share-point-google.c:103
msgid "Failed to upload photo: Service not authorized"
msgstr "שגיאה בהעלאת התמונה: שרת לא מורשה"

#: src/photos-share-point-google.c:105
msgid "Failed to upload photo"
msgstr "שגיאה בהעלאת התמונה"

#: src/photos-source-manager.c:465
msgid "Sources"
msgstr "מקורות"

#: src/photos-source-notification.c:171
msgid "New device discovered"
msgstr "זוהה התקן חדש"

#: src/photos-source-notification.c:184
msgid "Import…"
msgstr "ייבוא…"

#. Translators: %s refers to an online account provider, e.g.,
#. * "Facebook" or "Flickr".
#.
#: src/photos-source-notification.c:214
#, c-format
msgid "Your %s credentials have expired"
msgstr "תוקף האישור שלך עבור %s פג"

#: src/photos-source-notification.c:221
msgid "Settings"
msgstr "הגדרות"

#: src/photos-thumbnailer.c:74
msgid "D-Bus address to use"
msgstr "כתובת D-Bus לשימוש"

#: src/photos-tool-colors.c:386
msgid "Contrast"
msgstr "ניגודיות"

#: src/photos-tool-colors.c:407
msgid "Blacks"
msgstr "שחורים"

#: src/photos-tool-colors.c:428
msgid "Saturation"
msgstr "רוויה"

#: src/photos-tool-colors.c:447
msgid "Shadows"
msgstr "צל"

#: src/photos-tool-colors.c:466
msgid "Highlights"
msgstr "פרטים בהירים"

#: src/photos-tool-colors.c:495
msgid "Colors"
msgstr "צבעים"

#: src/photos-tool-crop.c:192
msgid "Free"
msgstr "חופשי"

#: src/photos-tool-crop.c:193
msgid "Original"
msgstr "גודל מקורי"

#: src/photos-tool-crop.c:194
msgid "1×1 (Square)"
msgstr "1×1 (ריבוע)"

#: src/photos-tool-crop.c:195
msgid "10×8 / 5×4"
msgstr "10×8 / 5×4"

#: src/photos-tool-crop.c:196
msgid "4×3 / 8×6 (1024×768)"
msgstr "4×3 / 8×6 (1024×768)"

#: src/photos-tool-crop.c:197
msgid "7×5"
msgstr "7×5"

#: src/photos-tool-crop.c:198
msgid "3×2 / 6×4"
msgstr "3×2 / 6×4"

#: src/photos-tool-crop.c:199
msgid "16×10 (1280×800)"
msgstr "16×10 (1280×800)"

#: src/photos-tool-crop.c:200
msgid "16×9 (1920×1080)"
msgstr "16×9 (1920×1080)"

#: src/photos-tool-crop.c:1459
msgid "Lock aspect ratio"
msgstr "נעילת יחס ממדים"

#: src/photos-tool-crop.c:1529
msgid "Landscape"
msgstr "לרוחב"

#: src/photos-tool-crop.c:1540
msgid "Portrait"
msgstr "לאורך"

#: src/photos-tool-crop.c:1551
msgid "Reset"
msgstr "איפוס"

#: src/photos-tool-crop.c:1569
msgid "Crop"
msgstr "חיתוך"

#: src/photos-tool-enhance.c:197
msgid "Sharpen"
msgstr "חידוד"

#: src/photos-tool-enhance.c:218
msgid "Denoise"
msgstr "הפחתת רעש"

#: src/photos-tool-enhance.c:245
msgid "Enhance"
msgstr "שיפור"

#. Translators: "None" refers to the nop magic filter when editing.
#: src/photos-tool-filters.c:182
msgctxt "Edit Filter"
msgid "None"
msgstr "ללא"

#: src/photos-tool-filters.c:189
msgid "1947"
msgstr "1947"

#: src/photos-tool-filters.c:196
msgid "Calistoga"
msgstr "קליסטוגה"

#: src/photos-tool-filters.c:202
msgid "Trencin"
msgstr "טרנצ׳ין"

#: src/photos-tool-filters.c:209
msgid "Mogadishu"
msgstr "מוגדישו"

#: src/photos-tool-filters.c:215
msgid "Caap"
msgstr "קאפ"

#: src/photos-tool-filters.c:222
msgid "Hometown"
msgstr "עיר מולדת"

#: src/photos-tool-filters.c:239
msgid "Filters"
msgstr "מסננים"

#: src/photos-tracker-controller.c:174
msgid "Unable to find Tracker on your operating system"
msgstr "לא נתן למצוא את Tracker במערכת ההפעלה שלך"

#: src/photos-tracker-controller.c:176
msgid "Unable to fetch the list of photos"
msgstr "לא ניתן לקבל את רשימת התמונות"

#~ msgid "It lets you:"
#~ msgstr "הוא מאפשר לך:"

#~ msgid "Send photos to remote DLNA renderers"
#~ msgstr "שליחת תמונות להתקני DLNA מרוחקים"

#~ msgid "Name your first album"
#~ msgstr "שם האלבום הראשון שלך"

#~ msgid "You can add your online accounts in %s"
#~ msgstr "ניתן להוסיף אותך לחשבונות מקוונים ב%s"

#~ msgid "Untitled Photo"
#~ msgstr "תמונה ללא כותרת"

#~ msgid "Cancel"
#~ msgstr "ביטול"

#~ msgid ""
#~ "Copyright © 2013 Intel Corporation. All rights reserved.\n"
#~ "Copyright © 2014 – 2015 Pranav Kant\n"
#~ "Copyright © 2012 – 2016 Red Hat, Inc."
#~ msgstr ""
#~ "‏Copyright © 2013 Intel Corporation. כל הזכויות שמורות.\n"
#~ "Copyright © 2014 – 2015 Pranav Kant\n"
#~ "Copyright © 2012 – 2016 Red Hat, Inc."

#~ msgid "About"
#~ msgstr "על אודות"

#~ msgid "Quit"
#~ msgstr "יציאה"

#~ msgid "Brightness"
#~ msgstr "בהירות"

#~ msgid "Width"
#~ msgstr "רוחב"

#~ msgid "Screen"
#~ msgstr "מסך"

#~ msgid "Golden ratio"
#~ msgstr "יחס הזהב"

#~ msgid "A paper sizes"
#~ msgstr "גדלי נייר"

#~ msgid "%.2f"
#~ msgstr "%.2f"

#~ msgid "Print the current photo"
#~ msgstr "הדפסת התמונה הנוכחית"

#~ msgid "A3 (297 × 420 mm)"
#~ msgstr "‏A3 ‏(297 × 420 מ״מ)"

#~ msgid "A4 (210 × 297 mm)"
#~ msgstr "‏A4 ‏(210 × 297 מ״מ)"

#~ msgid "1977"
#~ msgstr "1977"

#~ msgid "Brannan"
#~ msgstr "Brannan"

#~ msgid "Gotham"
#~ msgstr "Gotham"

#~ msgid "Gray"
#~ msgstr "אפור"

#~ msgid "Nashville"
#~ msgstr "Nashville"

#~ msgid "%a, %d %B %Y  %X"
#~ msgstr "%a, ה־%e ב%B %Y  %X"

#~ msgid "%.1f (lens)"
#~ msgstr "%.1f (עדשה)"

#~ msgid "%.1f (35mm film)"
#~ msgstr "%.1f (סרט 35 מ״מ)"

#~ msgid "Transformation on unloaded image."
#~ msgstr "התמרה על תמונה שלא נטענה."

#~ msgid "Transformation failed."
#~ msgstr "ההתמרה נכשלה."

#~ msgid "EXIF not supported for this file format."
#~ msgstr "אין תמיכה ב־EXIF במבנה קובץ זה."

#~ msgid "Image loading failed."
#~ msgstr "טעינת התמונה נכשלה."

#~ msgid "No image loaded."
#~ msgstr "לא נטענה תמונה."

#~ msgid "You do not have the permissions necessary to save the file."
#~ msgstr "אין לך את ההרשאות המתאימות לשמירת קובץ זה."

#~ msgid "Temporary file creation failed."
#~ msgstr "יצירת הקובץ הזמני נכשלה."

#~ msgid "At least two file names are equal."
#~ msgstr "לפחות שניים משמות הקבצים זהים."

#~ msgid "Could not display help for Image Viewer"
#~ msgstr "לא ניתן להציג את העזרה למציג התמונות"

#~ msgid " (invalid Unicode)"
#~ msgstr " (יוניקוד שגוי)"

#~ msgid "Recent"
#~ msgstr "אחרונים"

#~ msgid "No Favorites Found"
#~ msgstr "לא נמצאו מועדפים"

#~ msgid "Load More"
#~ msgstr "טעינת עוד"

#~ msgid "Loading…"
#~ msgstr "בטעינה…"

#~ msgid ""
#~ "<li>View recent local and online photos</li> <li>Access your Flickr "
#~ "content</li> <li>Send photos to remote DLNA renderers</li> <li>Set as "
#~ "background</li> <li>Print photos</li> <li>Select favorites</li> <li>Allow "
#~ "opening full featured editor for more advanced changes</li>"
#~ msgstr ""
#~ "<li>הצגת תמונות מקומיות ומרוחקות אחרונות</li> <li>גישה לתוכן Flickr שלך</"
#~ "li> <li>שליחת תמונות לעיבוד DLNA מרוחק</li>\n"
#~ "<li>קביעה כרקע שולחן העבודה</li> <li>הדפסת תמונות</li> <li>בחירת מועדפות</"
#~ "li> <li>אפשרות לפתיחה בעורך מלא לשינויים מתקדמים</li>"

#~ msgid "Facebook"
#~ msgstr "Facebook"

#~ msgid "Flickr"
#~ msgstr "Flickr"

#~ msgid "System Settings"
#~ msgstr "הגדרות מערכת"
