gnome-photos (44.0-3) unstable; urgency=medium

  * Stop using dh-sequence-gnome
  * Depend on localsearch instead of tracker & tracker-extract
  * Bump Standards Version to 4.7.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sat, 08 Mar 2025 14:23:59 -0500

gnome-photos (44.0-2.1) unstable; urgency=medium

  * Non-maintainer upload
  * Disable photos-test-pipeline (Closes: #1040202)

  [ Amin Bandali ]
  * Change packaging branch to debian/latest

  [ Simon McVittie ]
  * d/control, d/rules: Stop generating d/control from a template
  * d/control: Remove unused build-dependency on libgdata-dev
    (Closes: #1082013)

 -- Bastian Germann <bage@debian.org>  Fri, 03 Jan 2025 20:35:55 +0100

gnome-photos (44.0-2) experimental; urgency=medium

  * Includes changes from 43.0-2 accidentally left out of previous upload

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 07 Mar 2023 13:28:41 -0500

gnome-photos (44.0-1) experimental; urgency=medium

  * New upstream release
  * debian/control.in: Bump minimum meson to 0.60.0

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 07 Mar 2023 12:43:57 -0500

gnome-photos (43.0-2) unstable; urgency=medium

  * Team upload
  * d/control.in: Drop Recommends on dleyna-renderer, which was removed
    from Debian.
    This will make apt frontends less likely to try to keep dleyna-renderer
    installed, which can interfere with the libgupnp 1.6 transition.
    (Closes: #1030123)
  * d/p/dlna-Don-t-show-a-warning-if-dleyna-isn-t-installed.patch:
    Add patch to silence a warning on startup when dleyna is not installed,
    as it will usually be the case in Debian 12
  * d/control.in: Add Recommends on gvfs-fuse.
    It's needed when connecting to an iPhone, and possibly other devices
    accessed via gphoto2.
    Thanks to Diego Escalante (Closes: #963722)
  * d/control.in: Drop commented-out references to Grilo.
    We can get these back from VCS history if we need them.
  * Update standards version to 4.6.2 (no changes needed)

 -- Simon McVittie <smcv@debian.org>  Tue, 31 Jan 2023 15:15:59 +0000

gnome-photos (43.0-1) unstable; urgency=medium

  [ Simon McVittie ]
  * d/control.in: Drop alternative B-D on libgdk-pixbuf2.0-dev

  [ Jeremy Bicha ]
  * New upstream release
  * Drop Flickr removal patch: applied in new release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 20 Sep 2022 11:28:33 -0400

gnome-photos (43~beta-3) unstable; urgency=medium

  * Switch to libsoup3 and bump required gnome-online-accounts & geocode-glib

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 05 Sep 2022 18:54:40 -0400

gnome-photos (43~beta-2) unstable; urgency=medium

  * debian/control.in: Drop dependency on gnome-online-miners

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 23 Aug 2022 12:46:38 -0400

gnome-photos (43~beta-1) unstable; urgency=medium

  * New upstream release
  * Drop Facebook removal patch: applied in new release
  * Add patch to keep using older geocode-glib API version for now
  * Cherry-pick patch to drop Flickr plugin avoiding grilo+libsoup2
  * debian/control.in: Drop dependency on grilo
  * debian/control.in: Build-Depend on libportal-gtk3-dev
  * debian/control.in: Bump minimum libexiv2 to 0.14.0

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 23 Aug 2022 12:02:06 -0400

gnome-photos (42.0-2) unstable; urgency=medium

  * Cherry-pick patch to drop Facebook support since gfbgraph hasn't
    been ported to libsoup3
  * debian/control.in: Don't Build-Depend on gfbgraph

 -- Jeremy Bicha <jbicha@ubuntu.com>  Tue, 02 Aug 2022 15:19:21 -0400

gnome-photos (42.0-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.6.0
  * Drop test and meson patches: applied in new release

 -- Jeremy Bicha <jbicha@ubuntu.com>  Mon, 21 Mar 2022 13:40:03 -0400

gnome-photos (40.0-3) unstable; urgency=medium

  * Team upload
  * d/p/meson-remove-incorrect-args-for-i18n.merge_file.patch:
    Add patch from upstream to fix FTBFS with Meson 0.61 (Closes: #1005569)

 -- Simon McVittie <smcv@debian.org>  Sun, 13 Feb 2022 17:07:21 +0000

gnome-photos (40.0-2) unstable; urgency=medium

  [ Laurent Bigonville ]
  * debian/control.in: Switch BD from libgdk-pixbuf2.0-dev to
    libgdk-pixbuf-2.0-dev

  [ Jeremy Bicha ]
  * Add epoch to gegl dependency
  * debian/rules: Drop unneeded -Wl,--as-needed
  * d/rules: Override dh_fixperms to set correct permissions on /usr/libexec
  * Release to unstable

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 31 Aug 2021 18:06:45 -0400

gnome-photos (40.0-1) experimental; urgency=medium

  [ Iain Lane ]
  * New upstream release
  * control: Bump deps per upstream
  * control, rules: Bump to compat 13. Drop dh_missing --fail-missing
    override as this is now the default.
  * patches/
    - test-gegl-Mark-as-slow-test.patch: Rebase
    - gegl-Don-t-exceed-arbitrary-maximum-number-of-threads.patch: Drop. This
      is upstream (slightly modified).
    - Revert-Switch-to-private-instances-of-the-Tracker-2.x-dae.patch: Drop.
      We're switching to Tracker 3 now

  [ Laurent Bigonville ]
  * debian/control.in: Tighten gnome-photos-tests dependency against
    gnome-photos

 -- Iain Lane <iain.lane@canonical.com>  Thu, 24 Jun 2021 17:38:59 +0100

gnome-photos (3.38.0-3) experimental; urgency=medium

  [ Laurent Bigonville ]
  * debian/watch: Update to follow the new GNOME version scheme
  * debian/control: Do not restrict dleyna-renderer on linux-any anymore
  * Drop d/p/tests-keep-using-python2.patch, switch the test to python3.
    Also switch the dependencies of gnome-photos-tests to python3 and add
    some missing dependencies.
  * d/t/installed-tests: Fix running the installed tests.
    D-Bus activated services needs the DISPLAY to be set, start the
    dbus-daemon after xvfb. Override HOME and point it to AUTOPKGTEST_TMP

  [ Jeremy Bicha ]
  * Add gnome-photos-tests package containing installed tests from upstream
    and switch autopkgtest to run it using gnome-desktop-testing

 -- Laurent Bigonville <bigon@debian.org>  Fri, 14 May 2021 22:02:07 +0200

gnome-photos (3.38.0-2) unstable; urgency=medium

  * Team upload

  [ Simon McVittie ]
  * Add patch to avoid exceeding gegl's arbitrary max number of threads,
    fixing FTBFS on machines with more than 128 cores (Closes: #976932)

  [ Jeremy Bicha ]
  * d/rules: Remove leftovers from dual Autotools/Meson build system

 -- Simon McVittie <smcv@debian.org>  Fri, 01 Jan 2021 13:21:33 +0000

gnome-photos (3.38.0-1) unstable; urgency=medium

  * Team upload
  * New upstream release
  * Add patch to revert use of private Tracker daemons.
    GNOME is moving to Tracker 3.x by default, but Debian still has
    Tracker 2.x (see #964376) so we don't need this change yet.

 -- Simon McVittie <smcv@debian.org>  Mon, 05 Oct 2020 09:32:51 +0100

gnome-photos (3.34.2-1) unstable; urgency=medium

  * Team upload
  * New upstream release
    - Stop calling functions deprecated in gegl 0.4.23, fixing FTBFS and
      autopkgtest failure (Closes: #963683)
    - Fix test failures with newer babl
    - Translation updates

 -- Simon McVittie <smcv@debian.org>  Thu, 25 Jun 2020 10:08:06 +0100

gnome-photos (3.34.1-2) unstable; urgency=medium

  * Build-Depend on librsvg2-common:native.  This package contains the SVG
    backend for gdk-pixbuf, which is necessary to ensure that
    gdk-pixbuf-pixdata can actually load SVGs. It was previously pulled in via
    the dependency chain libgtk-3-0 -> adwaita-icon-theme -> librsvg2-common
    (Closes: #959584)

 -- Iain Lane <laney@debian.org>  Wed, 13 May 2020 10:43:29 +0100

gnome-photos (3.34.1-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in: Bump Standards-Version to 4.5.0 (no further changes)
  * debian/control.in: Add Rules-Requires-Root: no

 -- Laurent Bigonville <bigon@debian.org>  Thu, 12 Mar 2020 19:35:19 +0100

gnome-photos (3.34.0-2) unstable; urgency=medium

  * Add test-gegl-Mark-as-slow-test.patch:
    Mark gegl test as 'slow' so that it hopefully won't timeout on armel

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 20 Oct 2019 06:56:12 -0400

gnome-photos (3.34.0-1) unstable; urgency=medium

  * New upstream release

 -- Andreas Henriksson <andreas@fatal.se>  Wed, 18 Sep 2019 22:34:48 +0200

gnome-photos (3.32.0-1) experimental; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 19 Mar 2019 20:38:53 -0400

gnome-photos (3.31.91-1) experimental; urgency=medium

  * New upstream development release
  * Build with meson
  * Bump Build-Depends: libgdk-pixbuf2.0-dev 2.36.8 and libglib2.0-dev 2.57.2
  * Build-Depend on debhelper-compat 12 and drop debian/compat
  * Build-Depend on dh-sequence-gnome

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 21 Feb 2019 13:18:33 -0500

gnome-photos (3.30.1-2) unstable; urgency=medium

  * Add -Wl,-O1 to our LDFLAGS
  * Bump Standards-Version to 4.3.0

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 25 Dec 2018 08:18:39 -0500

gnome-photos (3.30.1-1) unstable; urgency=medium

  * New upstream release

 -- Tim Lunn <tim@feathertop.org>  Fri, 28 Sep 2018 16:35:45 +1000

gnome-photos (3.30.0-1) unstable; urgency=medium

  * New upstream release

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 06 Sep 2018 14:11:55 -0400

gnome-photos (3.29.92-1) unstable; urgency=medium

  * New upstream release
  * Bump libgexiv2-dev to >= 0.10.8 & libgrilo-0.3-dev to >= 0.3.5
  * Add X-Ubuntu-Use-Langpack to opt in to Ubuntu language pack handling
    (LP: #1779574)
  * Bump Standards-Version to 4.2.1
  * Drop patches applied in new release:
    - bump-minimum-GEGL-version-to-0.4.0.patch
    - correct_test_name.patch
    - unbreak-empty-trash-button.patch

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 05 Sep 2018 14:08:01 -0400

gnome-photos (3.28.0-2) unstable; urgency=medium

  * Cherry-pick bump-minimum-GEGL-version-to-0.4.0.patch
  * Bump minimum gegl to 0.4.0
  * Bump Standards-Version to 4.1.4

 -- Jeremy Bicha <jbicha@debian.org>  Sat, 28 Apr 2018 09:08:09 -0400

gnome-photos (3.28.0-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * New upstream release
  * Cherry-pick unbreak-trash-button.patch:
    - Fix Empty Trash button with GNOME 3.26+

  [ Sebastien Bacher ]
  * Add correct_test_name.patch:
    - update the app name in the test
  * debian/tests/control:
    - Depend on dleyna-server and dleyna-renderer.
      These are already Recommends of gnome-photos.

 -- Jeremy Bicha <jbicha@debian.org>  Tue, 27 Mar 2018 12:26:38 -0400

gnome-photos (3.27.92-1) unstable; urgency=medium

  * New upstream release
  * Release to unstable

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 14 Mar 2018 20:39:29 -0400

gnome-photos (3.27.90-1) experimental; urgency=medium

  [ Tim Lunn ]
  * Team upload
  * New upstream development release
  * debian/control.in: Add build-deps on libdazzle-1.0-dev
    and libgdk-pixbuf-2.0-dev, bump build-dep on libgegl-dev

  [ Pedro Beja ]
  * debian/control.in: Update description

 -- Tim Lunn <tim@feathertop.org>  Sun, 18 Feb 2018 14:39:53 +1100

gnome-photos (3.26.3-1) unstable; urgency=medium

  * New upstream release
  * Update Vcs fields for migration to https://salsa.debian.org/
  * Bump debhelper compat to 11

 -- Jeremy Bicha <jbicha@debian.org>  Thu, 01 Feb 2018 09:00:48 -0500

gnome-photos (3.26.2-2) unstable; urgency=medium

  [ Simon McVittie ]
  * Increase Priority to optional. Priority: extra is deprecated.

  [ Jeremy Bicha ]
  * Update Vcs fields for conversion to git
  * Add debian/gbp.conf
  * Bump Standards-Version to 4.1.2

 -- Jeremy Bicha <jbicha@debian.org>  Fri, 15 Dec 2017 12:48:15 -0500

gnome-photos (3.26.2-1) unstable; urgency=medium

  * New upstream release

 -- Michael Biebl <biebl@debian.org>  Fri, 03 Nov 2017 11:56:31 +0100

gnome-photos (3.26.1-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.1.1

 -- Jeremy Bicha <jbicha@debian.org>  Sun, 08 Oct 2017 10:04:16 -0400

gnome-photos (3.26.0-1) unstable; urgency=medium

  * New upstream release
  * debian/copyright: Minor updates
  * debian/control.in:
    - Build-Depend on tracker2 instead of tracker1
    - Drop obsolete Build-Depends on intltool
  * Drop build-with-tracker1.patch: Obsolete

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 13 Sep 2017 14:29:33 -0400

gnome-photos (3.25.91-1) unstable; urgency=medium

  * New upstream release
  * debian/control.in:
    - Bump minimum GTK+ to 3.22.16
    - Bump minimum GEGL to 0.3.15
    - Build-Depend on gnome-settings-dev instead of libgnome-desktop-3-dev
    - Build-depend on libgeocode-glib-dev
    - Depend on tracker-extract for gsettings schemas
  * Update debian/copyright
  * Add build-with-tracker1.patch:
    - Don't build with tracker 2 yet
  * Bump Standards-Version to 4.1.0

 -- Jeremy Bicha <jbicha@debian.org>  Wed, 30 Aug 2017 17:21:03 -0400

gnome-photos (3.22.6-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Fri, 30 Jun 2017 17:15:34 +0200

gnome-photos (3.22.5-1) unstable; urgency=medium

  * New upstream release.
  * Drop debian/patches/drop-jp2-gegl-check.patch, merged upstream.

 -- Michael Biebl <biebl@debian.org>  Thu, 09 Mar 2017 01:19:49 +0100

gnome-photos (3.22.4-1) unstable; urgency=medium

  * New upstream release.
  * Drop patches which have been merged upstream.

 -- Michael Biebl <biebl@debian.org>  Tue, 21 Feb 2017 23:36:53 +0100

gnome-photos (3.22.3-2) unstable; urgency=medium

  * debian/patches/0001-tests-Wait-for-the-UI-to-be-drawn-completely.patch:
    Take patch from upstream bug #777053 to resolve race condition in
    testsuite.
  * debian/tests/unit-tests: Don't fail on expected stderr from dbus-daemon.

 -- Iain Lane <laney@debian.org>  Mon, 09 Jan 2017 18:55:47 +0000

gnome-photos (3.22.3-1) unstable; urgency=medium

  * New upstream release.
  * Use dbus-run-session to run tests. (Closes: #835886)

 -- Michael Biebl <biebl@debian.org>  Tue, 03 Jan 2017 13:53:27 +0100

gnome-photos (3.22.2-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Sun, 06 Nov 2016 20:17:35 +0100

gnome-photos (3.22.1-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Thu, 06 Oct 2016 18:35:33 +0200

gnome-photos (3.22.0-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Sat, 17 Sep 2016 20:21:31 +0200

gnome-photos (3.21.92-1) unstable; urgency=medium

  * New upstream development release.
  * Refresh patches, drop the ones that were merged upstream.
  * Bump debhelper compat level to 10.

 -- Michael Biebl <biebl@debian.org>  Mon, 12 Sep 2016 02:45:58 +0200

gnome-photos (3.21.91-2) unstable; urgency=medium

  [ Jeremy Bicha ]
  * Add debian/patches/git_fix-tracker*.patch:
    - Add 3 patches from git to fix syntax errors with Tracker 1.9.1

 -- Michael Biebl <biebl@debian.org>  Wed, 07 Sep 2016 02:57:02 +0200

gnome-photos (3.21.91-1) unstable; urgency=low

  * New upstream beta release.
  * Update build-dependencies according to configure.ac changes:
    - bump glib to >= 2.44.0

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 02 Sep 2016 10:47:57 +0200

gnome-photos (3.20.3-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * debian/tests/control: Switch from grilo 0.2 to 0.3 here also

  [ Andreas Henriksson ]
  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Thu, 11 Aug 2016 17:47:00 +0200

gnome-photos (3.20.2-1) unstable; urgency=medium

  [ Jeremy Bicha ]
  * debian/docs, debian/rules:
    - Install NEWS but don't install other files that duplicate the
      copyright file

  [ Michael Biebl ]
  * Drop --disable-silent-rules from debian/rules. This is now handled by dh
    directly depending on whether the DH_QUIET environment variable is set.
  * New upstream release.
  * Bump Standards-Version to 3.9.8.

 -- Michael Biebl <biebl@debian.org>  Wed, 22 Jun 2016 22:40:13 +0200

gnome-photos (3.20.1-1) unstable; urgency=medium

  [ Andreas Henriksson ]
  * New upstream release.
  * Update build-dependencies according to configure.ac changes:
    - Drop libexempi-dev
    - Drop libexif-dev
    - Drop liblcms2-dev
    - Drop librsvg2-dev
    - Add libgexiv2-dev
    - Bump libgegl-dev to >= 0.3.5
    - Switch from libgrilo-0.2-dev to libgrilo-0.3-dev
    - Add libjpeg-dev
    - Add libpng-dev
    - Bump libgtk-3-dev to >= 3.19.1
  * Bump Standards-Version to 3.9.7
  * Recommend grilo-plugins-0.3 for grl-flickr plugin

  [ Michael Biebl ]
  * Drop uploaders.mk from debian/rules as this breaks the clean target with
    dh. Instead use the gnome dh addon which updates debian/control via
    dh_gnome_clean.
  * Tighten Build-Depends on libpng-dev to ensure we have a version >= 1.6.
  * Fix typo in package description.
  * Drop jp2 gegl check which prevents gnome-photos to start if GEGL was not
    compiled against libjapser. Not starting at all is worse then crashing
    when trying to open a jp2 file, as JPEG-2000 files are very rare. That
    said, we should fix the case anyway when gegl does not support a specific
    image format.

 -- Michael Biebl <biebl@debian.org>  Tue, 19 Apr 2016 19:45:19 +0200

gnome-photos (3.18.2-1) unstable; urgency=medium

  * New upstream release.

 -- Michael Biebl <biebl@debian.org>  Wed, 11 Nov 2015 02:13:27 +0100

gnome-photos (3.18.1-1) unstable; urgency=medium

  * New upstream release.
  * Wrap dependencies.
  * Bump Standards-Version to 3.9.6.

 -- Michael Biebl <biebl@debian.org>  Mon, 12 Oct 2015 17:59:12 +0200

gnome-photos (3.18.0-2) unstable; urgency=medium

  * Upload to unstable.

 -- Michael Biebl <biebl@debian.org>  Wed, 07 Oct 2015 23:27:10 +0200

gnome-photos (3.18.0-1) experimental; urgency=medium

  * Replace obsolete gnome-common build-dependency with simply
    intltool (>= 0.50.1). It was previously pulled in by gnome-common.
  * New upstream release.

 -- Andreas Henriksson <andreas@fatal.se>  Fri, 02 Oct 2015 23:39:44 +0200

gnome-photos (3.16.2-1) experimental; urgency=medium

  * Make debian/watch only track stable releases.
  * New upstream release.
  * Bump build-dependencies according to configure.ac changes:
    - libcairo2-dev (>= 1.14.0)
    - libgegl-dev (>= 0.3.0)
    - libgtk-3-dev (>= 3.15.5)
  * Upload to experimental as the new gegl-0.3 is only available there yet.

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 28 Jul 2015 18:29:34 +0200

gnome-photos (3.14.2-1) unstable; urgency=medium

  [ Laurent Bigonville ]
  * debian/control.in: Only Recommends dleyna-renderer on linux architectures

  [ Josselin Mouette ]
  * New upstream translation and bugfix release.

 -- Josselin Mouette <joss@debian.org>  Fri, 05 Dec 2014 18:29:34 +0100

gnome-photos (3.14.0-1) unstable; urgency=medium

  [ Jackson Doak ]
  * New upstream release
  * debian/rules:
    - Disable silent rules so we can find hardening issues
    - Fully enable hardening
  * Add 90_drop-onlyshowin.patch so every desktop can use this
  * debian/control:
    - Bump b-dep on gtk to >= 3.13.2, glib to >= 2.39.3
    - Build-depend on libgdata-dev
  * Run tests as autopkgtest

  [ Andreas Henriksson ]
  * Recommend dleyna-renderer to be able to display photos on other
    DLNA renderers in your local network.

  [ Laurent Bigonville ]
  * debian/control.in: Add gnome-online-miners to the dependencies and bump
    tracker to a hard dependency.

 -- Laurent Bigonville <bigon@debian.org>  Sat, 27 Sep 2014 14:09:54 +0200

gnome-photos (3.12.1-1) unstable; urgency=medium

  * Initial packaging.

 -- Andreas Henriksson <andreas@fatal.se>  Tue, 15 Jul 2014 16:57:46 +0200
